package storage

import "gitlab.com/fannyhasbi/article-service/model"

type ArticleStorage struct {
	ArticleMap map[string]model.Article
}

func CreateArticleStorage() *ArticleStorage {
	return &ArticleStorage{
		ArticleMap: make(map[string]model.Article),
	}
}
